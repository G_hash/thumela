'user strict';

const Branch = require("../Models/Branch");

//create branches

exports.create_a_branch = function(req, res) {
    var new_branch = new Branch(req.body);

    //handles null error 
    if (!new_branch.branch || !new_branch.status) {

        res.status(400).send({ error: true, message: 'Please provide branch/status' });

    } else {

        Branch.createBranch(new_branch, function(err, branch) {

            if (err)
                res.send(err);
            res.json(branch);
        });
    }
};



//update Branches
exports.update_a_branch = (req, res) => {
    Branch.updateBranch(req.params.id, new Branch(req.body), function(err, branch) {
        if (err) res.send(err);
        res.json(branch);
    });
};

//Delete Branches
exports.delete_a_branch = (req, res) => {
    Branch.deleteBranch(req.params.id, function(err, branch) {
        if (err) res.send(err);
        res.json({ message: "Branch successfully deleted" });
    });
};

//List Branches
exports.list_a_branch = (req, res) => {
    Branch.listBranch(function(err, branch) {
        console.log('controller');
        if (err) res.send(err);
        console.log("res", branch);
        res.send(branch);
    });
};
// Search branches
exports.search_a_branch = (req, res) => {
    Branch.searchBranch(req.params.id, function(err, branch) {
        if (err) res.send(err);
        res.json(branch);
    });
};