'user strict';
const express = require('express');
const User = require('../Models/User');

const app = express();




//User Registration
app.post('/register', async(req, res) => {
    const user = new User({
        name: req.body.name,
        surname: req.body.surname,
        password: req.body.password,
        cellphone: req.body.cellphone,
        email: req.body.email
    });

    try {
        const savedUser = await user.save();
        res.json(savedUser);
    } catch {
        res.status(400).send(err);
    }
});

module.exports = app;