'user strict';

const Roles = require('../Models/Roles');

//create branch
exports.create_a_role = function(req, res) {
    var new_roles = new Roles(req.body);

    if (!new_roles.roles || new_roles.status) {
        res.status(400).send({ error: true, message: 'please provide roles' });
    } else {
        Roles.createRoles(new_roles, function(err, roles) {
            if (err) res.send(err);
            res.json(roles);
        });
    }
};



//update roles
exports.update_a_role = (req, res) => {
    Roles.updateRoles(req.params.id, new Roles(req.body), function(err, roles) {
        if (err) res.send(err);
        res.json(roles);
    });
};

//delete roles
exports.delete_a_role = (req, res) => {
    Roles.deleteRoles(req.params.id, function(err, roles) {
        if (err) res.send(err);
        res.json({ message: 'Role deleted successfully' });
    });
};

//list Roles
exports.list_a_Role = (req, res) => {
    Roles.listRoles(function(err, roles) {
        console.log('controller');
        if (err) res.send(err);
        console.log('res', roles);
        res.send(roles);
    });
};

//Search ROles
exports.search_a_role = (req, res) => {
    Roles.searchRoles(req.params.id, function(err, roles) {
        if (err) res.send(err);
        res.json(roles);
    });
};