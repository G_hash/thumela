'user strict';

const User = require('../Models/User');

//create business
exports.create_a_user = function(req, res) {
    var new_user = new User(req.body);

    //handles null
    if (!new_user.user || new_user.status) {
        res.status(400).send({ error: true, message: 'Please provide User' });
    } else {
        User.createUser(new_user, function(err, user) {
            if (err) res.send(err);
            res.json(user);
        });
    }
};

//Update User
exports.update_a_user = (req, res) => {
    User.updateUser(req.params.id, new User(req.body), function(err, user) {
        if (err) res.send(err);
        res.json(user);
    });
};

//delete user
exports.Delete_a_user = (req, res) => {
    User.deleteUser(req.params.id, function(err, user) {
        if (err) res.send(err);
        res.json({ message: 'User deleted successfully' });
    });
};

//list user
exports.list_a_user = (req, res) => {
    User.listUser(function(err, user) {
        console.log('controller');
        if (err) res.send(err);
        console.log('res :', user);
        res.json(user);
    });
};

//Sreach User
exports.search_a_user = (req, res) => {
    User.searchUser(req.params.id, function(err, user) {
        if (err) res.send(err);
        res.json(user);
    });
};