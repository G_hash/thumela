"user strict";

const db = require("../database"); //importing database

//Branch Schema
const Branch = function(newBranch) {
    console.log("newBranch :", newBranch);
    this.branchid = newBranch.branchid;
    this.branchname = newBranch.branchname;
    this.branchcreated = new Date();
};

Branch.createBranch = function(newBranch, result) {
    db.query("INSERT INTO branches SET ", newBranch, function(err, res) {
        if (err) {
            console.log("error :", err);

            result(err, null);
        } else {
            console.log("resultID :", res.insertId);

            result(null, res.insertId);
        }
    });
};

//Update branch
Branch.updateBranch = function(id, editedBranch, result) {
    db.query("UPDATE branches SET branchname = ? WHERE branchid = ?", [editedBranch, id], function(err, res) {
        if (err) {
            console.log('error :', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//Delete Branch
Branch.deleteBranch = function(id, result) {
    db.query("DELETE FROM branches WHERE branchid = ?", [id], function(err, res) {
        if (err) {
            console.log("error :", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

// list branches
Branch.listBranch = function(result) {
    db.query("SELECT * FROM branches", function(err, res) {
        if (err) {
            console.log('error :', err);
            result(null, err);
        } else {
            console.log("Branch :", res);
            result(null, res);
        }
    });
};

//Search Branch
Branch.searchBranch = function(id, result) {
    db.query("SELECT branchname FROM branches WHERE branchid = ?", [id], function(err, res) {
        if (err) {
            console.log("error : ", err)
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Branch;