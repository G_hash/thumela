"user strict";

const db = require("../database");

//roles SChema
const Roles = function(newRoles) {
    console.log("newBranchv :", newRoles);
    this.roleid = newRoles.roleid;
    this.rolename = newRoles.rolename;
    this.roledescription = newRoles.roledescription;
    this.rolecreateddate = new Date();
};

Roles.createRoles = function(newRoles, result) {
    db.query("INSERT INTO roles SET ? ", newRoles, function(err, res) {
        if (err) {
            console.log("error", err);
            result(err, null);
        } else {
            console.log('resultid :', res.insertId);
            result(null, res.insertId);
        }
    });
};


//update roles
Roles.updateRoles = function(id, editedRoles, result) {
    db.query("UPDATE roles SET rolename = ?, roledescription=? WHERE roleid = ?", [editedRoles.rolename, editedRoles.roledescription, id], function(err, res) {
        if (err) {
            console.log("error", err);
            result(err, null);
        } else {
            result(null, res);
        }
    });
};

//delete Roles
Roles.deleteRoles = function(id, result) {
    db.query("DELETE FROM roles WHERE roleid = ?", [id, ], function(err, res) {
        if (err) {
            console.log('error', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//list
Roles.listRoles = function(result) {
    db.query('SELECT * FROM roles', function(err, res) {
        if (err) {
            console.log('error', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//search roles
Roles.searchRoles = function(id, result) {
    db.query('SELECT * FROM roles WHERE roleid = ?', [id], function(err, res) {
        if (err) {
            console.log('error', err)
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Roles;