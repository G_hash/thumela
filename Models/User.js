'user strict';
const db = require('../database');

const User = function(newUser) {
    console.log('newUser :', newUser);
    this.userid = newUser.userid;
    this.branchid = newUser.branchid;
    this.roleid = newUser.roleid;
    this.name = newUser.name;
    this.surname = newUser.surname;
    this.password = newUser.password;
    this.cellphone = newUser.cellphone;
    this.email = newUser.email;
    this.registrationdate = new Date();
    this.profilepicture = newUser.profilepicture;
};

//Creating User
User.createUser = (newUseR, result) => {
    db.query('INSERT INTO user SET?', newUseR, function(err, res) {
        if (err) {
            console.log('error', err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};

//Search User
User.searchUser = function(id, result) {
    db.query('SELECT * FROM user WHERE userid = ?', [id], function(err, res) {
        if (err) {
            console.log('error: ', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//Delete User
User.deleteUser = function(id, result) {
    db.query('DELETE FROM user WHERE userid = ?', [id], function(err, res) {
        if (err) {
            console.log('error :', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//list User
User.listUser = function(result) {
    db.query('SELECT * FROM user', function(err, res) {
        if (err) {
            console.log('error', err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

//Update User
User.updateUser = function(id, editUser, result) {
    db.query('UPDATE user SET  ?', [editUser.name, editUser.surname, editUser.password, editUser.cellphone, editUser.email, id], function(err, res) {
        if (err) {
            console.log('error', err);
            result(null, err);
        } else {
            result(null, res)
        }
    });
};

module.exports = User;