'user strict';

var mysql = require('mysql');

//creating database connection
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "1234",
    database: "thumela"
});

connection.connect();

module.exports = connection;