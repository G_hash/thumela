const bcrypt = require('bcryptjs');
const generator = require('generate-password');
const jwt = require('jsonwebtoken');

const thumelaEncrypt = function(value) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(value, salt);
    return hash;
};

const thumelaVerify = function(password, hash) {
    return bcrypt.compareSync(password, hash);
};

const verifyToken = function(req, res, next) {
    //Request header with authorization key
    const bearerHeader = req.header['authorization'];

    //Check if there is a header
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');

        //Ger Token array by splitting
        const bearerToken = bearer[1];
        req.token = bearerToken;
        //call next middleware
        next();
    } else {
        res.sendStatus(403);
    }
};

const generatepassword = generator.generate({
    length: 8,
    numbers: true
});

module.exports = {
    thumelaEncrypt,
    thumelaVerify,
    generatepassword
};