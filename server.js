'user strict';

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const Branch = require("./Controllers/BranchController");
const Roles = require("./Controllers/RolesController");
const User = require('./Controllers/UserControl');
const Registration = require('./Controllers/RegistrationController');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", () => {
    console.log("Welome to Thumela Restful api")
});

//Branches Routes
app.route('/api/branch').get(Branch.list_a_branch); //Branch List
app.route('/api/branch/:id').get(Branch.search_a_branch); //Branch Search
app.route('/api/branch/:id').delete(Branch.delete_a_branch); //Branch Delete
app.route('/api/branch/:id').put(Branch.update_a_branch); //branch update
app.route('/api/branch').post(Branch.create_a_branch); //branch creation

//Roles Routes
app.route('/api/roles').get(Roles.list_a_Role); //Role List
app.route('/api/roles/:id').get(Roles.search_a_role); //Role search
app.route('/api/roles/:id').delete(Roles.delete_a_role); //Role Delete
app.route('/api/roles/:id').put(Roles.update_a_role); //roles delete
app.route('/api/roles').post(Roles.create_a_role); //Roles creation

//User Routes
app.route('/api/user').get(User.list_a_user); //User list
app.route('/api/user/:id').get(User.search_a_user); //User Search
app.route('/api/user/:id').delete(User.Delete_a_user); //User delete
app.route('/api/user/:id').put(User.update_a_user); //User update
app.route('/api/user').post(User.create_a_user);

//Account
app.use('/api/account', Registration);

//request not found
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//all other request are not found
app.use((err, req, next) => {
    res.status(err.status || 501);
    res.json({
        error: {
            code: err.status || 501,
            message: err.message
        }
    });
});

const port = process.env.port || 3000;
app.listen(port, "127.0.0.1", console.log("Server is ruuning on port 3000"))